import React, { useState, useEffect } from "react";
import { createUseStyles } from "react-jss";

import ComboBoxItem from "./ComboBoxItem";
import Item from "./Item";

const useStyles = createUseStyles({
  container: {
    position: "relative",
    display: "inline-block",
  },
  dropdown: {
    position: "absolute",
    maxHeight: "10em",
    width: "100%",
    overflow: "auto scroll",
    marginTop: "0",
    padding: "0",
    listStyle: "none",

    boxShadow: "0.2em 0.2em 0.3em #556",
    borderRadius: "0.5em",
    border: "1px solid #88d",
    backgroundColor: "#fff",
    ["&.hidden"]: {
      display: "none",
    },
  },
});


interface IProps<T> {
  options: Array<T>;
  render?: (item: T) => (JSX.Element | string);
  label_for?: (item: T) => string;
  key_for?: (item: T) => (string | number);
  on_soft_select?: (item: T) => void;
  on_harden: (item: T) => void;
  max_displayed_options?: number;
}

/**
 * ComboBox, a guided input element
 */
export default function ComboBox<T extends Item>(props: IProps<T>): JSX.Element {
  const {
    options,
    on_harden,
    label_for = item => (item?.label || item?.name || ""),
    key_for = item => (item?.id || item?.label || item?.name),
    render = null,
    on_soft_select = null,
    max_displayed_options = 10,
  } = props;
  const classes = useStyles();

  const [text, set_text] = useState("");
  const [is_dropped_down, set_is_dropped_down] = useState(false);

  const filtered_options = options.filter(
    i => label_for(i).toLowerCase().includes(text.toLowerCase())
  ).slice(0, max_displayed_options);

  const max_selection = filtered_options.length - 1;
  const [selection, set_selection] = useState<number | null>(null);

  useEffect(function(): void {
    // Do nothing if there is no `on_soft_select` function
    if (!on_soft_select) return;

    // Trigger callback if something is selected (`selection` can be `undefined`)
    if (selection !== null)
      if (filtered_options[selection])
        on_soft_select(filtered_options[selection]);
  }, [selection, text]);

  /**
   * Catch control keys (arrow keys, <Enter>, <Esc>) pressed in the input box
   */
  function onKeyDown(e: React.KeyboardEvent): void {
    switch (e.key) {
    case "ArrowUp":
      set_is_dropped_down(true);
      if (selection === null || selection <= 0)
        set_selection(max_selection);
      else
        set_selection(selection - 1);
      break;

    case "ArrowDown":
      set_is_dropped_down(true);
      if (selection === null || selection >= max_selection)
        set_selection(0);
      else
        set_selection(selection + 1);
      break;

    case "Enter":
      // Do nothing if there's no selection
      if (selection !== null) {
        const item = filtered_options[selection];
        on_harden(item);
        set_text(label_for(item));
        set_is_dropped_down(false);
      }
      break;

    case "Escape":
      set_is_dropped_down(false);
      break;
    }
  }

  /**
   * Callback for typing text in the input
   */
  function onTextChange(e: React.ChangeEvent<HTMLInputElement>): void {
    set_is_dropped_down(true);
    set_text((e.target as HTMLInputElement).value);
    if ((e.target as HTMLInputElement).value)
      set_selection(0);
  }

  const ul_classes = [classes.dropdown];
  if (!is_dropped_down)
    ul_classes.push("hidden");

  return (
    <div className={classes.container}>
      <input
        type="text"
        value={text}
        onKeyDown={onKeyDown}
        onChange={onTextChange}
        onFocus={() => set_is_dropped_down(true)}
        onBlur={() => set_is_dropped_down(false)}
      />
      <ul className={ul_classes.join(" ")}>
        {filtered_options.map((v, i) => (
          <ComboBoxItem key={key_for(v)} is_selected={i === selection}>
            { (render || label_for)(v) }
          </ComboBoxItem>
        ))}
      </ul>
    </div>
  );
}
