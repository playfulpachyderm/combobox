import React, { useRef } from "react";
import { createUseStyles } from "react-jss";

const useStyles = createUseStyles({
  dropdownItem: {
    paddingLeft: "0.5em",
    boxSizing: "border-box",
    lineHeight: "1.6em",
    borderTop: "1px solid #dedede",
  },
  dropdownItemSelected: {
    borderRadius: "0.2em",
    border: "1px solid #aaaacc",
    backgroundColor: "#ddddff",
  },
});

interface IProps {
  is_selected?: boolean;
  children: JSX.Element | string;
}

/**
 * A list item (option) in a ComboBox dropdown
 */
export default function ComboBoxItem(props: IProps): JSX.Element {
  const { is_selected, children } = props;
  const classes = useStyles();

  const ref = useRef<HTMLLIElement | null>(null);

  const classlist = [classes.dropdownItem];
  if (is_selected) {
    classlist.push(classes.dropdownItemSelected);
    if (ref.current)
      ref.current.scrollIntoView();
  }
  return (
    <li className={classlist.join(" ")} ref={ref}>
      {children}
    </li>
  );
}
ComboBoxItem.defaultProps = {
  is_selected: false,
};
