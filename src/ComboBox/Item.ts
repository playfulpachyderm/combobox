interface Item {
  label?: string;
  name?: string;
  id?: string | number;
}

export default Item;
