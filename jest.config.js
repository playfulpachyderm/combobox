import path from "path";

const __dirname = path.dirname(new URL(import.meta.url).pathname);

export default {
  preset: 'ts-jest',
  testEnvironment: 'node',
  moduleDirectories: ["src", ".", "node_modules"],
  testMatch: [path.join(__dirname, "test/**/?(*.)+(spec|test).[jt]s?(x)")],
  transform: {
    '^.+\\.[jt]sx$': 'babel-jest',
  },
};
