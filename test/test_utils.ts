/**
 * Some helpful testing functions to easily test input elements on the fake
 * DOM.
 */

import { ReactTestInstance } from "react-test-renderer";
import Item from "ComboBox/Item";


/**
 * Set the text on an input element
 */
function set_text(element: ReactTestInstance, text: string): void {
  element.props.onChange({target: { value: text }});
}

/**
 * Run an `expect` assertion on the text of an input elemtn
 */
function expect_text(element: ReactTestInstance, text: string): void {
  expect(element.props.value).toBe(text);
}

/**
 * Assert equivalence between objects
 */
function expect_json_equivalent(o1: Item, o2: Item): void {
  expect(JSON.stringify(o1)).toEqual(JSON.stringify(o2));
}

/**
 * Assert a ComboBox list item is selected or not
 */
function expect_selected(li: ReactTestInstance, truth: boolean = true): void {
  const exp = expect(li.props.className.toLowerCase());
  (truth ? exp : exp.not).toMatch("selected");
}

/**
 * Assert selection for a group of ComboBox list items
 */
function expect_only_selected(li_list: Array<ReactTestInstance>, index: number): void {
  for (let i = 0; i < li_list.length; ++i)
    expect_selected(li_list[i], i === index);
}

/**
 * Assert the dropdown is hidden or not
 */
function expect_hidden(ul: ReactTestInstance, truth: boolean = true): void {
  const exp = expect(ul.props.className.toLowerCase());
  (truth ? exp : exp.not).toMatch("hidden");
}

export { set_text, expect_text, expect_json_equivalent, expect_selected, expect_only_selected, expect_hidden };
